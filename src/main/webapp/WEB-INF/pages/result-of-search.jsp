<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <link href="${pageContext.request.contextPath}/css/mystyle.css" rel="stylesheet" >


  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Список пользователей</title>



</head>
<body>
<center>
<h1>Резльтаты поиска</h1>
<p>  <b>Найдено пользователей: ${sizeresult} </b></p>

<a href="${pageContext.request.contextPath}/search-init.html" class="button" >Новый поиск</a>
<a href="${pageContext.request.contextPath}/list.html" class="button" >Назад</a>

<br/>
<br/>
<div id="pagination">

  <c:url value="/result" var="prev" >
    <c:param name="page" value="${page-1}"/>
  </c:url>


  <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
    <c:choose>
      <c:when test="${page == i.index}">
        <span>${i.index}</span>
      </c:when>
      <c:otherwise>
        <c:url value="/result" var="url">
          <c:param name="page" value="${i.index}"/>
        </c:url>
        <a href='<c:out value="${url}" />'>${i.index}</a>
      </c:otherwise>
    </c:choose>
  </c:forEach>
  <c:url value="/result" var="next">
    <c:param name="page" value="${page + 1}"/>
  </c:url>

</div>
</br>

<table  class="bordered">
  <thead>
  <tr>
<th width="50px">id</th><th width="100px">Имя</th><th width="100px">Возраст</th><th width="50px">Админ</th><th width="200px">Действия</th>
  </tr>
  </thead>
  <tbody>
  <c:forEach var="user" items="${users}">
    <tr>
      <td>${user.id}</td>
      <td>${user.name}</td>
      <td>${user.age}</td>
      <td>${user.isAdmin}</td>

		<td>
		<a href="${pageContext.request.contextPath}/edit/${user.id}.html" class="button" >Редактировать</a>
		<a href="${pageContext.request.contextPath}/delete/${user.id}.html" class="button buttonRed" >Удалить</a>
	</td>

    </tr>
  </c:forEach>


  </tbody>
</table>

  <br/>
  <br/>



</center>
</body>
</html>